import discord, time, asyncio, random
from discord.ext import commands
from Code.Core.cogs import Cogs
from datetime import datetime

class Event(Cogs):
    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        channel = reaction.message.channel
        channel = str(channel)
        if channel == "一般":
            if str(user) == "OpenBot#8009":
                return
            await reaction.message.remove_reaction(reaction, user)
            await user.send(f"<@{user.id}> 您已成功取得翻譯員身分")
            role = discord.utils.get(user.guild.roles, name="翻譯員")
            await user.add_roles(role)
            print(user, "已獲得翻譯員身分")
    
    @commands.Cog.listener()
    async def on_command_error(ctx, error):
        if isinstance(error, commands.CommandNotFound):
            embed = discord.Embed(
                title=f"你似乎輸入了錯誤的指令，查看可用指令請使用 {prefix}help 指令", colour=discord.Colour.red())
            await ctx.send(embed=embed)
            await asyncio.sleep(1)
        else:
            embed = discord.Embed(
                title=f"執行指令時發生錯誤：\n{error}", colour=discord.Colour.red())
            await ctx.send(embed=embed)
            
def setup(bot):
    bot.add_cog(Event(bot))
