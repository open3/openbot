import discord, time, asyncio, random
from discord.ext import commands
from Code.Core.cogs import Cogs
from datetime import datetime

class General(Cogs):
    @commands.command(name="echo", description='使用語法：echo [內容] | 使用 OpenBot 說話')
    async def echo(self, ctx, *arg):
        if ctx.message.author.guild_permissions.administrator:
            await ctx.message.delete()
            await ctx.send(" ".join(arg))

    @commands.command()
    async def ping(self, ctx):
        now = datetime.now()
        embed = discord.Embed(color=0x3939ff)
        embed.set_author(name=f"延遲時間：{round((self.bot.latency * 1000), 2)} 毫秒(ms)", icon_url="https://gitlab.com/open3/openbot/-/raw/master/Res/Icon/ping.png")
        embed.set_footer(text=f"{now.year}/{now.month}/{now.day} 由 {str(ctx.author)[:5]} 提起 | /ping 指令")
        await ctx.send(embed = embed)
    
    @commands.command(name='clock', description='使用語法：clock [時區(預設 0)] | 查看時間')
    async def clock(self, ctx, num=0):
        now = datetime.now()
        embed = discord.Embed(color=0x09de93)
        embed.set_author(name=f"現在時間：{now.hour+num} 點 {now.minute} 分 {now.second} 秒", icon_url="https://gitlab.com/open3/openbot/-/raw/master/Res/Icon/clock.png")
        embed.set_footer(text=f"{now.year}/{now.month}/{now.day} 由 {str(ctx.author)[:5]} 提起 | /clock 指令")
        await ctx.send(embed=embed)
    
    @commands.command(name='clean', description='使用語法：clean [模式(all)/數字] | 清理訊息')
    async def clean(self, ctx, mode: str):
        now = datetime.now()
        embed = discord.Embed(color=0xeafa0f)
        try:
            mode = int(mode)
            await ctx.channel.purge(limit=mode+1)
            embed.set_author(name=f"你已清除 {mode} 條訊息！", icon_url="https://gitlab.com/open3/openbot/-/raw/master/Res/Icon/clean.png")
            embed.set_footer(text=f"{now.year}/{now.month}/{now.day} 由 {str(ctx.author)[:5]} 提起 | /clean 指令")
        except:
            if mode == "all":
                await ctx.channel.purge(limit=None)
                embed.set_author(name=f"你已清除所有訊息！", icon_url="https://gitlab.com/open3/openbot/-/raw/master/Res/Icon/clean.png")
                embed.set_footer(text=f"{now.year}/{now.month}/{now.day} 由 {str(ctx.author)[:5]} 提起 | /clock 指令")
            else:
                return
        await ctx.send(embed=embed)
        time.sleep(1.25)
        await ctx.channel.purge(limit=1)
    

    @commands.command(name='dice', description='骰子功能，可以投擲骰子 1-6 號')
    async def dice(self, ctx):
        now = datetime.now()
        english_dice = ["one", "two", "three", "four", "five", "six"]
        await ctx.channel.purge(limit=1)
        num = random.randint(1, 6)
        embed = discord.Embed(color=0xbf0202)
        embed.set_author(name=f"你骰到了 {num}", icon_url="https://raw.githubusercontent.com/open3/OpenBot/master/Res/dice{}.png".format(english_dice[num - 1]))
        embed.set_footer(text=f"{now.year}/{now.month}/{now.day} 由 {str(ctx.author)[:5]} 提起 | /dice 指令")
        await ctx.send(embed=embed)
    

    @commands.command(name='time', description='使用語法：time [模式（hour/min/sec）] [數字]')
    async def time(self, ctx, mode, num):
        now = datetime.now()
        english_dice = ["one", "two", "three", "four", "five", "six"]
        await ctx.channel.purge(limit=1)
        num = random.randint(1, 6)
        embed = discord.Embed(color=0xbf0202)
        embed.set_author(name=f"你骰到了 {num}", icon_url="https://raw.githubusercontent.com/open3/OpenBot/master/Res/dice{}.png".format(english_dice[num - 1]))
        embed.set_footer(text=f"{now.year}/{now.month}/{now.day} 由 {str(ctx.author)[:5]} 提起 | /dice 指令")
        await ctx.send(embed=embed)
    

    @commands.command()
    async def rule(self, ctx):
        if ctx.message.author.guild_permissions.administrator:
            reaction = "✅"
            embed=discord.Embed(description="尊重 包容 友善\n嚴禁與他人進行語言與文化上的歧視\n不入戰 不引戰 是尊重人的基本原則\n", color=0xbc2494)
            embed.set_author(name="伺服器規則", icon_url="https://image.flaticon.com/icons/svg/760/760205.svg")
            embed.add_field(name="----------------", value="1. 請勿騷擾或謾罵別人\n\n這是你我都懂的道理吧", inline=False)
            embed.add_field(name="----------------", value="2. 聊天區不限任何話題，我們有一定程度尊重你的言論自由\n\n但我們嚴禁宣揚任何廣告 色情內容及政治言論", inline=False)
            embed.add_field(name="----------------", value="3. 發言前請先為你提及的人或族群著想\n\n勿罵偏激及歧視性言論", inline=False)
            embed.set_footer(text="2020/02/19 最後修訂，製作 By.open3")
            msg = await ctx.send(embed=embed)
            await msg.add_reaction(reaction)
          


def setup(bot):
    bot.add_cog(General(bot))
