import discord, os, json, asyncio
from discord.ext import commands
from Code.Core.menu import Pages
from tkinter import *

with open("./Data/token.json", "r", encoding="utf8") as jfile:
    token = json.load(jfile)
with open("./Data/info.json", "r", encoding="utf8") as jfile:
    data = json.load(jfile)
with open("./Data/version.json", "r", encoding="utf8") as jfile:
    ver = json.load(jfile)
    

def boot():
    global data
    global ver
    if data["FAST_BOOT"] == "FALSE":
        root.destroy()
    ver = ver["VER"]
    prefix = data["PREFIX"]
    name = data["NAME"]

    class PaginatedHelpCommand(commands.HelpCommand):
        async def PaginateMenu(self, entries, per_page=10, show_entry_count=True, numsbool=True, embedcolor='blurple', thumbnail=None):
            PMenuConsole = Pages(self.context, entries=entries, per_page=per_page,
                                show_entry_count=show_entry_count, numsbool=numsbool, embedcolor=embedcolor, thumbnail=thumbnail)
            await PMenuConsole.paginate()

        async def send_bot_help(self, mapping):
            messages = [f"**📚  {name} 指令大全　|　{ver}\n**", 
                        F"🌀  {prefix}clear [數量]\n", "清除訊息\n",
                        F"🎲  {prefix}dice\n", "骰子功能，可以投擲骰子 1-6 號\n",
                        F"💬  {prefix}echo [內容]\n", F"使用 {name} 的名義來發言\n",
                        F"📙  {prefix}help\n", "查詢本機器人的功能\n",
                        F"🤖  {prefix}openbot\n", "查詢本機器人的官方網站\n",
                        F"🌐  {prefix}ping\n", "查詢你的網路延遲度（稱為 Ping 值）\n",
                        F"{prefix}reload\n", "重新載入 OpenBot\n",]
            await self.PaginateMenu(entries=messages, numsbool=False, embedcolor='orange', thumbnail="https://raw.githubusercontent.com/open3/OpenBot/master/Res/report.png")

        async def send_command_help(self, command):
            messagess = []
            descbool = True
            helpbool = True
            if command.description:
                descbool = True
            else:
                descbool = False
            if command.help:
                helpbool = True
            else:
                helpbool = False

            if descbool and helpbool:
                messagess.append(f'{command.description}\n\n{command.help}')
            elif descbool:
                messagess.append(f'{command.description}')
            elif helpbool:
                messagess.append(f'{command.help}')
            await self.PaginateMenu(entries=messagess, numsbool=False, embedcolor='orange')

        async def command_not_found(self, string):
            allcmd = set(command.name for command in client.walk_commands())
            allcmd = list(allcmd)
            temp = difflib.get_close_matches(string, allcmd, n=1, cutoff=0.65)
            if not temp:
                suggestCommand = 'No other possible commands'
            else:
                suggestCommand = temp[0]
            embed = discord.Embed(
                title="**Invalid Command Passed to $help!!!**", colour=discord.Colour.red())
            embed.add_field(name=f"We couldnt find the help page of {string}", value=f'Error: Command does not exist',
                            inline=False)
            if not suggestCommand == 'No other possible commands':
                embed.add_field(name=f"We suggest you to try the following command instead:",
                                value=f'{prefix}help {suggestCommand}', inline=False)
            embed.set_thumbnail(
                url="https://icon-library.net/images/close-icon-png/close-icon-png-19.jpg")
            await self.context.send(embed=embed)

    bot = commands.Bot(command_prefix="/", help_command=PaginatedHelpCommand())

    @bot.event
    async def on_ready():
        print(">> Bot is Online <<")

    @bot.command()
    async def reload(ctx):
        for filename in os.listdir("./Code"):
            if filename.endswith(".py"):
                bot.reload_extension(f"Code.{filename[:-3]}")
        await ctx.send("重新載入成功")

    for filename in os.listdir("./Code"):
        if filename.endswith(".py"):
            bot.load_extension(f"Code.{filename[:-3]}")

    bot.run(token["TOKEN"])

def update_dcpy():
    Label1 = Label(root, text="　　　[正在更新 Discord.py]　　　")
    Label1.pack()
    Label1 = Label(root, text="　　　[更新失敗/原因：連接 Git 失敗]　　　")
    Label1.pack()

def edit():
    edit_screen = Tk()
    edit_screen.title("更改機器人資料")
    edit_screen.mainloop()

if data["FAST_BOOT"] == "TRUE":
    boot()
else:
    root = Tk()
    root.title("OpenBot 控制中心")
    Label(root, text="\n　　　歡迎來到 OpenBot 控制中心　　　\n").pack()
    Button(root, text="開啟 OpenBot", command=boot).pack()
    Button(root, text="安裝/更新 Discord.py", command=update_dcpy).pack()
    # Button(root, text="更改機器人資訊", command=edit).pack()
    Label(root, text="\n").pack()
    root.mainloop()
